extends LinkButton

export(String) var scene_to_load

func _on_StartGame_pressed():
	get_tree().change_scene(str("res://Scenes/Level1/" + scene_to_load + ".tscn"))
