extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 800

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	var animation = "diri_kanan"
	velocity.x = 0
	if Input.is_action_pressed('right'):
		velocity.x += speed
		animation = "jalan_kanan"
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		animation = "jalan_kiri"
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
	if Input.is_action_pressed("exit"):
		get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))

func _physics_process(delta):
  velocity.y += delta * GRAVITY
  get_input()
  velocity = move_and_slide(velocity, UP)

