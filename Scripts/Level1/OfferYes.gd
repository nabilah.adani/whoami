extends Button

export(String) var scene_to_load

func _on_IYA_pressed():
	get_tree().change_scene(str("res://Scenes/Level1/" + scene_to_load + ".tscn"))
