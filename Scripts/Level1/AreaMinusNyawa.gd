extends Area2D

export (String) var sceneName = "Level1"

func _on_MinusNyawa_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		if (global.lives == 0):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else :
			global.lives -=1
			get_tree().change_scene(str("res://Scenes/Level1/" + sceneName + ".tscn"))
		
