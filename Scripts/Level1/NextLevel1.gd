extends Area2D

export(String) var sceneName = "Level2"

func _on_NextLevel_body_entered(body):
	if (global.kunci == 4) && (global.serpihan == 1):
		get_tree().change_scene(str("res://Scenes/Level2/" + sceneName  + ".tscn"))
