extends Node2D

var rand = RandomNumberGenerator.new()
onready var new_scene = load("res://Scenes/Level1/Jagung2D.tscn")

export (NodePath) var timerPath
var timer = Timer.new()

func _ready():
	timer.start()

func _on_Timer_timeout():
	var jagung = new_scene.instance()
	rand.randomize()
	var x = rand.randf_range(20, 896)
	jagung.position.x = x
	add_child(jagung) 
	timer.start()
