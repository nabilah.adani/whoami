extends Button

export(String) var scene_to_load

func _on_TIDAK_pressed():
	if (global.lives == 0):
		get_tree().change_scene(str("res://Scenes/Level1/DeathNo.tscn"))
	else :
		global.lives -= 1
		get_tree().change_scene(str("res://Scenes/Level1/" + scene_to_load + ".tscn"))
