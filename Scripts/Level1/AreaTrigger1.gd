extends Area2D

export (String) var sceneName = "Level1"

func _on_AreaTrigger2_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/Level1/" + sceneName + ".tscn"))
