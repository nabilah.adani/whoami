extends Area2D

export (String) var sceneName = "Challenge2a"

func _on_MinusNyawa_body_entered(body):
	if body.get_name() == "Player":
		if (global.lives == 0):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else :
			global.lives -=1
			global.kunci = 0 
			get_tree().change_scene(str("res://Scenes/Level2/" + sceneName + ".tscn"))

