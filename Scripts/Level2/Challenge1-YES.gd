extends Button

export(String) var scene_to_load

func _on_IYA_pressed():
	if (global.lives == 0):
		get_tree().change_scene(str("res://Scenes/Level2/DeathNo2.tscn"))
	else :
		global.lives -= 1
		get_tree().change_scene(str("res://Scenes/Level2/" + scene_to_load + ".tscn"))
