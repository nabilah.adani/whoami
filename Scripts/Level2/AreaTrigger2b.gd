extends Area2D

export (String) var sceneName = "Level2b"

func _on_AreaTrigger_body_entered(body):
	if body.get_name() == "Player":
		if (global.permen == 2):
			get_tree().change_scene(str("res://Scenes/Level2/" + sceneName + ".tscn"))
		else :
			pass 
