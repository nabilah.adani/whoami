extends Button

export(String) var scene_to_load

func _on_OKE_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
